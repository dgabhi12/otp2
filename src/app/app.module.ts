import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { AppComponent } from './app.component';
import {MatCardModule} from '@angular/material/card';
import{MatMenuModule} from '@angular/material/menu';
import{MatIconModule} from '@angular/material/icon';
import{MatProgressBarModule} from '@angular/material/progress-bar';
import{MatFormFieldModule} from '@angular/material/form-field';
import{MatToolbarModule} from '@angular/material/toolbar';
import { CountdownModule } from 'ngx-countdown';

@NgModule({
  imports:      [ BrowserModule,CountdownModule, FormsModule,MatCardModule,MatToolbarModule ,MatSlideToggleModule,MatMenuModule,MatIconModule,MatProgressBarModule,MatFormFieldModule],
  declarations: [ AppComponent],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
